Studentnummer: 331665
Naam: Rob van Grieken

Verschilt de Terminal in Visual Studio Code met Git Bash?
	# Nee

Waar worden Branches vaak voor gebruikt?
	# Feature / Bugfix

Hoe vaak ben je in Opdracht 5A van Branch gewisseld?
	# 2

Vul de volgende commando's aan:
 -Checken op welke branch je aan het werken bent:
	# git branch
 -Nieuwe Branch aanmaken
	# git branch "Naam van je nieuwe branch"
 -Van Branch wisselen
	# git checkout "Naam van de branch waar je heen wilt"
 -Branch verwijderen
	# git branch -d "Naam van de Branch die je wilt verwijderen"
